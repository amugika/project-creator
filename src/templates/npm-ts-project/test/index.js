const lib = require('./../dist');

const expect = require('chai').expect;

describe('<%= projectName %> tests', () => {
    
    it('Suma correcta', () => {
        expect(1 + 1).to.equal(2);
    });
});