#!/usr/bin/env node

// Regex https://regexr.com/4deku
// 1)
const fs = require('fs');
const path = require('path');

// 7
const shell = require('shelljs');

// 6 Para renderizar argumentos en los ficheros
const render = require('./utils/templates').render;

const CHOICES = fs.readdirSync(path.join(__dirname, 'templates'));

const QUESTIONS = [
{
    name: 'template',
    type: 'list',
    message: 'What project template would you like to generate?',
    choices: CHOICES
},
{
    name: 'name',
    type: 'input',
    message: 'Project name:',
    validate: function (input) {
            if (/^([a-z@]{1}[a-z\-\.\\\/]{0,213})+$/.test(input)) return true;
            else return 'Project name may only include lowercase letters, numbers, underscores and hashes.';
    }
},
{
    name: 'author',
    type: 'input',
    message: 'Author:'
},
{
    name: 'github-user',
    type: 'input',
    message: 'Github User:'
},
{
    name: "license",
    type: "list",
    message: "Select project license",
    choices: ["MIT", "Apache", "GNU", "Mozilla", "ISC"]
}];

// 2) Instalar chalk y también inquirer
const inquirer = require('inquirer');
const chalk = require('chalk');

// 3) Add Current directory
const CURR_DIR = process.cwd();

inquirer.prompt(QUESTIONS)
.then(answers => {
    // Esto es con el 3)
    const projectChoice = answers['template'];
    const projectName = answers['name'];
    const licenseSelect = answers['license'];
    const authorName = answers['author'];
    const githubUser = answers['github-user'];

    const templatePath = path.join(__dirname, 'templates', projectChoice);
    const tartgetPath = path.join(CURR_DIR, projectName); 

    const options = {
        authorName,
        licenseSelect,
        projectName,
        templateName: projectChoice,
        templatePath,
        tartgetPath
    }

    // console.log(options);
    // Con el 3

    // 4 
    if (!createProject(tartgetPath)) {
        return;
    }

    // 5
    createDirectoryContents(templatePath, projectName, authorName, licenseSelect, githubUser);

    // Con el 2
    //console.log(answers);
    postProcess(options);
});

// 4 --> Probar y ejecutar
function createProject(projectPath) {
    if (fs.existsSync(projectPath)) {
        console.log(chalk.red(`Folder ${projectPath} exists. Delete or use another name.`));
        return false;
    }

    fs.mkdirSync(projectPath);
    
    return true;
}

// 5
const SKIP_FILES = ['node_modules', '.template.json'];

function createDirectoryContents(templatePath, projectName, authorName, licenseSelect, githubUser) {
  const filesToCreate = fs.readdirSync(templatePath);

  filesToCreate.forEach(file => {
    const origFilePath = path.join(templatePath, file);

    // get stats about the current file
    const stats = fs.statSync(origFilePath);

    if (SKIP_FILES.indexOf(file) > -1) return;

    if (stats.isFile()) {
      let contents = fs.readFileSync(origFilePath, 'utf8');

      // 6
      contents = render(contents, { projectName, authorName, licenseSelect, githubUser});

      const writePath = path.join(CURR_DIR, projectName, file);

      // EXTRA - Para ver la información del fichero mientras lo copia
      const CREATE = chalk.green('CREATE');
      const fileSize = stats["size"];
      console.log(`${CREATE} ${writePath} (${fileSize} bytes)`);

      fs.writeFileSync(writePath, contents, 'utf8');
    } else if (stats.isDirectory()) {
      fs.mkdirSync(path.join(CURR_DIR, projectName, file));

      // recursive call
      createDirectoryContents(path.join(templatePath, file), path.join(projectName, file));
    }
  });

}

function postProcess(options) {
    const isNode = fs.existsSync(path.join(options.templatePath, 'package.json'));

    // Proceso de instalación
    if (isNode) {
        shell.cd(options.tartgetPath);

        console.log(chalk.green(`Install need dependencies in ${options.tartgetPath}.`));
        const result = shell.exec('npm install');
        if (result.code !== 0) {
            return false;
        }
    }
    
    return true;
}